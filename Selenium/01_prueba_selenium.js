const {Builder, By, Key, until} = require('geckodriver');

(async function example() {
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    await driver.get('http://www.google.com/ncr');
    await driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
    await driver.wait(until.titleIs('webdriver - Buscar con Google'), 2000);
  } finally {
    await driver.quit();
  }

  //los parentesis ejecutan la funcion al momento
})();