using NUnit.Framework;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using OpenQA.Selenium.Interactions;

namespace _08_NUnit_Selenium
{
    public class TestsSeleniumFirefox
    {
        IWebDriver driver;

        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            // string fichFirefox = "../../../../FirefoxPortable/FirefoxPortable.exe";
            // string fichFirefox = "C:\\Users\\pmpcurso1\\AppData\\Local\\Mozilla Firefox\\firefox.exe";
            string fichFirefox = "../../../../FirefoxPortable/App/Firefox64/firefox.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if (File.Exists(fichFirefox))
            {
                // string rutaDriverFirefox = "geckodriver.exe";
                // FirefoxBinary binarioFirefox = new FirefoxBinary(fichFirefox);
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = fichFirefox;

                driver = new FirefoxDriver(firefoxOptions);
            }
        }


        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            // driver.Close();
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            driver.Navigate().GoToUrl("https://duckduckgo.com/");
            IWebElement textoBusq = driver.FindElement(By.Name("q"));
            textoBusq.SendKeys("SQL Tutorials w3school create tables");
            IWebElement botonBusq = driver.FindElement(By.Id("search_button_homepage"));
            botonBusq.Click();
            // var enlaces = driver.FindElements(By.CssSelector(".result__a"));
            var enlaces = driver.FindElements(By.CssSelector("a[href*='https://www.w3schools.com/']"));

            foreach (var enlace in enlaces)
            {
                if (enlace.Equals(enlaces[0]) && enlace.Displayed)  // Esto realmente se puede hacer en una l�nea
                {

                    enlace.Click();
                    break;
                }
            }
            Assert.GreaterOrEqual(enlaces.Count, 3, "No se han encontrado suficientes enlaces");

            driver.FindElement(By.Id("accept-choices")).Click();

            Actions accion = new Actions(driver);
            var enlaceSQL = driver.FindElement(By.CssSelector("a[href='sql_datatypes.asp']"));
            accion.MoveToElement(enlaceSQL);

            //for (int i = 0; i < 20; i++)
            //  accion.KeyDown(Keys.ArrowDown);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", enlaceSQL);
            Wait(3);
            enlaceSQL.Click();
            ///
            IWebElement tituloNumData = driver.FindElement(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table"));
            //IWebElement tituloNumData = driver.FindElement(By.XPath("//h3[text()='Numeric Data Types'][2]"));

            var filaDatatype = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[1]"));
            var filaStorage = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[3]"));
            int i = 0;

            // se podria comprobar que el numero de tablas es el correcto

            foreach (var item in filaDatatype)
            {
                Console.WriteLine(item.Text + "-" + filaStorage[i].Text);
                Console.WriteLine($"{item.Text}-{filaStorage[i]}");
                i++;
            }


            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumData);




            //Assert.IsNotNull
        }
        public void Wait(int sec, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, sec);
            var timeIni = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeIni) > delay);
        }
    }
}
