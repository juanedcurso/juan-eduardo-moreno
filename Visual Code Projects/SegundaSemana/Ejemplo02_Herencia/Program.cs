﻿using System;
using Ejemplo01_Encapsulacion;

namespace Ejemplo02_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche miCoche = new Coche("Seat", "Ibiza", 12000);
            Coche miCoche2 = new Coche("Seat", "Cordoba", 12000);
            miCoche.Acelerar();

            Console.WriteLine("To String 1: " + miCoche.ToString());
            Console.WriteLine("Hash code 1: " + miCoche.GetHashCode());
            Console.WriteLine("Hash code 2: " + miCoche2.GetHashCode());
            Console.WriteLine("Type: " + miCoche2.GetType().ToString());
            if (miCoche.GetType().Equals(typeof(Coche)))
            {
                Console.WriteLine("miCoche es un coche! ");
            }
            miCoche2.Modelo = "Ibiza";
            if (miCoche.Equals(miCoche2))
            {
                Console.WriteLine(miCoche.ToString() + 
                    " es igual al 2! ");
            } else
            {
                Console.WriteLine("miCoche es diferente al 2! ");
            }

            FuncionRecursiva(25);

            CocheElectrico miTesla = new CocheElectrico();
            miTesla.Modelo = "SX";
            miTesla.Marca = "Tesla";
            miTesla.NivelBateria = 100;

            Console.WriteLine(miTesla.ToString() );
            miTesla.Acelerar();
            miTesla.Acelerar();
            Console.WriteLine("Velocidad: " + miTesla.Velocidad);
            Console.WriteLine("NivelBateria: " + miTesla.NivelBateria);

            // Hacemos lo que se llama un casting implícito
            // usando el polimorfismo del lenguaje
            Coche miTeslaComoCoche = miTesla;
            // double x = 3.454F;
            // Lo que ocurre, es que con la forma de la clase padre
            // sólo podemos usar campos y métodos de la clase padre
            miTeslaComoCoche.Marca = "Tesla sencillito";
            // Aunque todavía almacene los datos de Coche Electrico,
            // no se pueden usar
            // ERROR: miTeslaComoCoche.NivelBateria = 34;
            Object miTeslaComoObjeto = miTesla;
            // ERROR: miTeslaComoObjeto.Marca = 34;
            Console.WriteLine("ToString(): " + miTeslaComoObjeto.ToString());
            miTeslaComoCoche.Acelerar();
            miTeslaComoCoche.Acelerar();
            miTesla.Acelerar(45);
            Console.WriteLine("Velocidad: " + miTesla.Velocidad);
            Console.WriteLine("NivelBateria: " + miTesla.NivelBateria);
        }
        static void FuncionRecursiva(int numero)
        {
            if (numero < 30)
            {
                Console.WriteLine(numero);
                FuncionRecursiva(numero + 1);

            }
        }
    }
}
