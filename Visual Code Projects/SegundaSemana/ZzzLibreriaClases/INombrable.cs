﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_Interfaces
{
    /* Como un contrato que las clases que la implementen tienen que cumplir
     Cualquier clase que implemente la interfaz (NO se dice que herede)
    debe tener programados todos sus métodos y propiedades)
    Las interfaces privadas (como las clases privadas) sólo se pueden
    usar dentro del proyecto (aplicación, librería DLL...)
    Si son públicas pueden usarse fuera del proyecto
 */

    /*
    Como un contrato que las clases que la implementen tienen que cumplir
    cualquier clase que implemente la interfaz (NO SE DICE QUE HEREDE)
    DEBE tener progframados todos sus metodos y propiedades

    Esta comentada el otro interfaz porque no podemos hacer referencias cruzadas entre proyectos.
    Entre objectos (claees) se puede hacer, pero tampoco se debe
    porque aumenta mucho la complejidad y lo que se llama deuda tecnica.

    Las interfaces privadas (como las clases privadas ) solo pueden usar dentro del proyecto ( aplicacion,libreria,DLL...)
    Si son publicas pueden usarse fuera del proyecto.


    */
    public interface INombrable
    {
        string GetNombre();

        void SetNombre(string unNombre);

        string Nombre
        {
            get;
            set;
        }
        //se puede crear un metodo pero NO SE DEBE, las interfaces por deficion son contratos, no clases.
        void MetodoQueSea() { Console.WriteLine("hacer cosas"); }
    }
}
