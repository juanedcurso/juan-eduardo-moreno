﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejemplo03_Interfaces;
using Ejercicio02_Herencia;
using System;

namespace Ejercicio_de_Interfaces
{
    class Program
    {

        /*  
               2 - Podeis un nuevo proyecto Ejercicio03_interfaces
                    Hacer que el usuario que ya tenemos implemente la interfaz 
                    INombrable y usar los 2 métodos y la propiedad
               3 - Crear un array de INombrable con un empleado y su coche electrico
               4 - Icloneable YA EXISTE en .net
                    Implementar dicha interfaz en CocheElectrico
                    El método tiene que instanciar(new por narices) un nuevo obj y asignar 
                    las propiedades del nuevo coche con sus propias propiedades
               5 - Crear una nueva interfaz que obligue a implementar 2 metodos, uno para mostrar directamente los datos
                    por consola y otro para pedir sus datos por consola.


               6 - Implementar dicha interfaz en Usuario, sobreecribir los metodos en empleado ( BONUS: y en coche electrico )
               7 - Usar los metodos en un nuevo usuario y en el empleado del ejercicio 3. y en un coche ( y si quereis CocheElectrico)
         
                 
           */

        static void Main(string[] args)

        {

            object cocheElectrico = new CocheElectrico("Fiat", "Punto", 9000);
            //De esta manera no haria falta castear el tipo mas abajo, linea 36
            //CocheElectrico cocheElectrico = new CocheElectrico("Fiat", "Punto", 9000);
            Empleado empleado = new Empleado("Carlos", 28, 1.69f, 25000);

            INombrable[] workerWithCar = new INombrable[2];
            workerWithCar[0] = empleado;
            workerWithCar[1] = (CocheElectrico)cocheElectrico;

            //falta trozo de codigo de german en empleado.
            //((Empleado)workerWithCar[0].SuCoche = (Coche)workerWithCar2[1];

            Console.WriteLine(workerWithCar[0].Nombre+" tiene "+ workerWithCar[1].Nombre);

            CocheElectrico mismoElectrico = (CocheElectrico) workerWithCar[1]; //necesario castearlo porque viene de inombrable
            CocheElectrico copiaElectrico = (CocheElectrico)((CocheElectrico)workerWithCar[1]).Clone(); //necesario castearlo porque viene de inombrable
            
            //si modificamos el clone, el original seguira siendo el mismo.

            CocheElectrico cocheElectrico2 = new CocheElectrico("marronero", "barato", 1);
            Empleado empleado2 = new Empleado("segundon", 28, 1.69f, 25000);
            IMostrar_mas_Pedir[] workerWithCar2 = new IMostrar_mas_Pedir[2];
            workerWithCar2[0] = empleado2;
            workerWithCar2[1] = (CocheElectrico)cocheElectrico2;

            
            workerWithCar2[0].Mostrar();
            workerWithCar2[1].Mostrar();

            Console.WriteLine("comprobando el punto 5");
            workerWithCar2[0].Pedir();
            workerWithCar2[1].Pedir();

            workerWithCar2[0].Mostrar();
            workerWithCar2[1].Mostrar();

            /*version de german 
             
            Console.WriteLine("INombrable!");
            INombrable[] varios = new INombrable[2];
            varios[0] = new Empleado("Pepito", 35, 1.8f, 20000);
            varios[1] = new CocheElectrico("BMW", "S2", 15000, 90);
            // Completamos la depencia unidireccional
            ((Empleado)varios[0]).SuCoche = (Coche) varios[1];

            foreach (INombrable innom in varios)
            {
                Console.WriteLine(innom.ToString());
            }

            CocheElectrico mismoElectrico = (CocheElectrico) varios[1];
            CocheElectrico copiaElectrico = (CocheElectrico) ((CocheElectrico)varios[1]).Clone();

            mismoElectrico.Modelo = "Nuevo modelo S2";
            Console.WriteLine("mismoElectrico = " + mismoElectrico.ToString());
            Console.WriteLine("varios[1] = " + varios[1].ToString());

            copiaElectrico.Modelo = "Copia nuevo modelo S2";
            Console.WriteLine("copiaElectrico = " + copiaElectrico.ToString());
            Console.WriteLine("varios[1] = " + varios[1].ToString());

            Coche cocheUsuario = new Coche();
            cocheUsuario.PedirDatos();
            cocheUsuario.MostrarDatos();            
             
             */
            Empleado empleadoUsuario = new Empleado();
            empleadoUsuario.PedirDatos();
            empleadoUsuario.MostrarDatos();

        }
    }
}
