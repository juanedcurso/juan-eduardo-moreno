﻿using System;

namespace Ejemplo06_CSharp_Avanzado.MetodosExt
{
    // En C# podemos añadir métodos a una clase desde otras clases:
    public static class ModuloMetodosExtension
    {
        // Mediante una función estática, con esta estructura:
        public static string FormatearNombre(this Fruta laFruta)
        {
            // Esto es un Método de Extensión
            string nombreFormateado = "FRUTA " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }
        // Mediante una función estática, con esta estructura:
        public static string FormatearNombre(this Fruta laFruta, string formato)
        {
            return formato + " " + laFruta.Nombre.ToUpper();
        }
        public static string ToUpperString(this object cualquierObjeto)
        {
            return cualquierObjeto.ToString().ToUpper();
        }
        public static string ToUpperString(this Array cualquierArray)
        {

            void Fun()
            {
                Console.WriteLine();
            }

            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
        public static string ToStringFiltrado(this Array cualquierArray, string cadenaFiltro)
        {


            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                if (elem.ToString().ToUpper().Contains(cadenaFiltro.ToUpper()))
                    result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
    }
}
