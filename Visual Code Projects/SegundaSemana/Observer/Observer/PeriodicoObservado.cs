﻿using System;
using System.Collections.Generic;

namespace Observer
{
    delegate void NoticiaCiencia(string not);
    class PeriodicoObservado
    {

        List<ISuscriptorObservador> listaSuscriptoresMA;
        List<ISuscriptorObservador> listaSuscriptoresHU;

        public NoticiaCiencia NuevaNotCiencia;
        public void NoticiaCiencia(string laNoticia)
        {
            if (NuevaNotCiencia != null)
            {
                NuevaNotCiencia($"Ciencia: {laNoticia}");
            }
        }


        public PeriodicoObservado()
        {
            listaSuscriptoresMA = new List<ISuscriptorObservador>();
            listaSuscriptoresHU = new List<ISuscriptorObservador>();
        }
        public void NotificarNoticiasMA()
        {
            foreach (var suscriptor in listaSuscriptoresMA)
            {
                suscriptor.ActualizarNotificacionNoticia();
            }
        }
        public void AddSuscriptorMA(ISuscriptorObservador observador)
        {
            listaSuscriptoresMA.Add(observador);
        }

        public void NotificarNoticiasHU()
        {
            foreach (var suscriptor in listaSuscriptoresHU)
            {
                suscriptor.ActualizarNotificacionNoticia();
            }
        }
        public void AddSuscriptorHU(ISuscriptorObservador observador)
        {
            listaSuscriptoresHU.Add(observador);
        }






        public void ArchivarNoticia(string titular, DateTime fecha, string tipo)
        {
            if (tipo.Equals("humano"))
            {
                System.IO.File.AppendAllText("noticias_humanos.txt", fecha.ToString() + " -> " + titular + "\n");
            }
            else System.IO.File.AppendAllText("noticias_maquinas.txt", fecha.ToString() + " -> " + titular + "\n");
        }

        public void DeleteSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptoresMA.Remove(observador);
        }
    }
}
