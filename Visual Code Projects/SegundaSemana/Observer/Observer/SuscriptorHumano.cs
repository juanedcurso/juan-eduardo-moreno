﻿using System;

namespace Observer
{
    class SuscriptorHumano : ISuscriptorObservador
    {

        string nombre;
        public SuscriptorHumano(string nombre)
        {
            this.nombre = nombre;
        }

        public void ActualizarNotificacionNoticia()
        {
            Console.Beep();
            Console.WriteLine("!Nuevas noticias! " + nombre + "! tus noticias de las " + DateTime.Now + " son:");

            foreach (var linea in System.IO.File.ReadLines("noticias_humanos.txt"))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(linea);
                Console.ForegroundColor = ConsoleColor.White;
            }

        }
    }
}
