﻿using System;
using System.Threading;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Patron observer: Periodico");
            PeriodicoObservado alDia = new PeriodicoObservado();

            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptorHU(juan);
            SuscriptorHumano alfonsito = new SuscriptorHumano("Alfonsito");
            alDia.AddSuscriptorHU(alfonsito);
            SuscriptorSkynet t1000 = new SuscriptorSkynet(1000);
            alDia.AddSuscriptorMA(t1000);
            SuscriptorSkynet t2000 = new SuscriptorSkynet(2000);
            alDia.AddSuscriptorMA(t2000);

            alDia.ArchivarNoticia("Bitcoin sube hasta los 100k", DateTime.Now, "humano");
            alDia.ArchivarNoticia("Bitcoin se estampa, vende ya !!", DateTime.Now, "humano");
            alDia.ArchivarNoticia("000010101 0101010 10010101", DateTime.Now, "maquina");
            alDia.ArchivarNoticia("000010101 0101010 10010101", DateTime.Now, "maquina");

            alDia.DeleteSuscriptor(t1000);
            bool cierre = false;
            DateTime lastma = System.IO.File.GetLastWriteTime("noticias_maquinas.txt");
            DateTime lasthu = System.IO.File.GetLastWriteTime("noticias_humanos.txt");

            while (!cierre)
            {
                DateTime newnotma = System.IO.File.GetLastWriteTime("noticias_maquinas.txt");
                DateTime newnothu = System.IO.File.GetLastWriteTime("noticias_humanos.txt");

                if (newnotma > lastma && System.IO.File.ReadAllText("noticias_maquinas.txt") != null)
                {
                    alDia.NotificarNoticiasMA();
                    System.IO.File.WriteAllText("noticias_maquinas.txt", "");
                    lastma = System.IO.File.GetLastWriteTime("noticias_maquinas.txt");
                }

                if (newnothu > lasthu && System.IO.File.ReadAllText("noticias_humanos.txt") != null)
                {
                    alDia.NotificarNoticiasHU();
                    System.IO.File.WriteAllText("noticias_humanos.txt", "");
                    lasthu = System.IO.File.GetLastWriteTime("noticias_humanos.txt");

                }

                Thread.Sleep(5000);

                //para que el siscriptor de ciencia se pueda suscribir se asigna 
                //la funcion al campo delegeado
                alDia.NuevaNotCiencia = NationalGeo;

                //si lo mostramos antes de poner esto no saldra nada porqu aun no ha ocurrido el evento
                // en el momento que "pase" algo saldra.
                alDia.NoticiaCiencia("cosas");

            }

            //este suscriptor solo es una funcion
            static void NationalGeo(string noticiaDePseudociencia)
            {
                Console.WriteLine("Los leones son territoriales " + noticiaDePseudociencia);
            }

            //System.IO.File.AppendAllText("fich_maquina.txt", "Ejemplo texto");
            //System.IO.File.WriteAllText("fich_maquina.txt", "Ejemplo texto");

            //5  ocurre otra noticia
            //6  humano desuscribe por mentiroso
            //7  ultima noticia
            //8  el periodico cierra
            //9  suscriptores reciban tambien la fecha y hora de la noticia y la muestren.

        }
    }
}
