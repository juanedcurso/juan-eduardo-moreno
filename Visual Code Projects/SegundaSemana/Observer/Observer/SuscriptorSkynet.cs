﻿using System;

namespace Observer
{
    class SuscriptorSkynet : ISuscriptorObservador
    {

        int id;

        public SuscriptorSkynet(int id)
        {
            this.id = id;
        }

        public void ActualizarNotificacionNoticia()
        {
            Console.Beep();
            Console.WriteLine("0000011 0101010 01010101 1010101010  " + id + "! 0001010 101001 10101010 " + DateTime.Now + " 10100110:");

            foreach (var linea in System.IO.File.ReadLines("noticias_maquinas.txt"))
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(linea);
                Console.ForegroundColor = ConsoleColor.White;
            }


        }
    }
}
