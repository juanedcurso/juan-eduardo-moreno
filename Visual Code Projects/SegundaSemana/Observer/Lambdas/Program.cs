﻿using System;

namespace Lambdas
{
    /*
     vamos a crear un nuevo tipo de dato que en vez de 
    almacenar informacion, almacene una funcion con 
    determinada estructura o firma 
    la estructura es como la interfaz de una funcion,
    pero sin importar el nombre: los tipos de datos que recibe y el tipo de da
    to que devuelve
    en C eran punteros a funciones en java hasta java8
    eran interfaces con una sola funcion, en JS funciones flecha
    en c# son los delegados
     
     
     
     */
    delegate void FuncionQueRecibeInt(int i);
    class Program
    {
        static void Main(string[] args)
        {
            //Lambdas
            //funciones estataticas

            FuncionEstaticas(5);
            OtraEstatica(7);
            // Variables de tipo funcion
            FuncionQueRecibeInt funRecInt;
            funRecInt = FuncionEstaticas;
            funRecInt(200);

            funRecInt = OtraEstatica;

            OtroSistema(OtraEstatica);
            OtroSistema(FuncionEstaticas);
            OtroSistema(funRecInt);

        }

        static void FuncionEstaticas(int x)
        {
            Console.WriteLine("No necesito un objeto para ser");
            Console.WriteLine("Param: " + x);
        }

        static void OtraEstatica(int x)
        {
            Console.WriteLine(x + " - Otra Estatica: ");

        }

        static void OtroSistema(FuncionQueRecibeInt funExt)
        {
            // queremos recibir una funcion como parametro:
            //llamarfuncionexterna por ejemplo que resiba un int
            int valor = 3;
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(2000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llama a la funcionalidad externa");
            funExt(3);


        }


    }
}
