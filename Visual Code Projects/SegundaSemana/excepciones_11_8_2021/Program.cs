﻿using System;

namespace Zzexcepciones_11_8_2021
{
    class Program
    {

        static void FuncionRecursivaInfinita()
        {

            FuncionRecursivaInfinita();
        }
        static void Main(string[] args)
        {
            

            try 
            {
                //aunque lo intentemos no podemos controlar la excepcion de stackoverflop
              //  FuncionRecursivaInfinita();
            } catch(Exception ex)
            {
                Console.Error.WriteLine("wololo");
            }

            try
            {
                Console.WriteLine("Provoquemos el caos!");
                int.Parse("caos");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("wololo"+ ex.Message);
            }


            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYmenos = null;
                string coso = nadaYmenos.ToString();

                Console.WriteLine(coso);
            }
            catch (NullReferenceException ex)
            {
                Console.Error.WriteLine("wololo" + ex.Message);
            }

            try
            {
                FuncionQuedelegaexcepcion();
                // la excepcion se progaga siempre hacia arriba hasta que un try catch adecuado la controle.

            }
            catch (FormatException ex)
            {
                Console.Error.WriteLine("error desde abajo" + ex.Message);
            }




        }
        public static void Otrafunncionquedelegaexcepcion()
        {
            // si no se conttrola se propaga hacia arriba
            try { FuncionQuedelegaexcepcion(); }
            catch (NullReferenceException ex){ }

        }

        public static void FuncionQuedelegaexcepcion()
        {
            //si una funcion lanza una excepcion y no la controlamos aqui podra o debera ser controlada en la funcion que invoca a esta
            int.Parse("error");

        }
    }
}
