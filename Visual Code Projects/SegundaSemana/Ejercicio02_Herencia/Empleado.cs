﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio02_Herencia
{ 
    public class Empleado : Ejercicio01_Encapsulacion.Usuario, IMostrar_mas_Pedir
    {
        float salario;


        public Empleado() : base("", 0, 0)
        {
            
        }

        public Empleado(string nombre, int edad, float altura, float salario) 
            : base(nombre, edad, altura)
        {
            this.Salario = salario;
        }

        public float Salario { get => salario; 
            set => salario = (value < 7000) ? 7000 : value ;
        }

        public override string ToString()
        {
            return base.ToString().Replace("Usuario", "Empleado") 
                + ", " + this.Salario + " EUR  ";
        }


        public void Mostrar()
        {
            Console.WriteLine("El empleado es: " + Nombre);
        }

        public void Pedir()
        {
            base.Pedir();
            Console.Write("Introduzca el salario: ");
            Salario = int.Parse(Console.ReadLine());
        }

        //para sobreescribir el metodo pedir datos de usuario.
        public override void PedirDatos()
        {
            base.PedirDatos();
            UIConsole.PedirNum("Salario", out salario);
        }

    }
}
