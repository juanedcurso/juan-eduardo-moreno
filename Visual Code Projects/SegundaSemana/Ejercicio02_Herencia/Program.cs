﻿using Ejercicio01_Encapsulacion;
using System;

/*
 * Ejercicio02_Herencia
 * Que use el Ejericio01_Enca...
 * CLase Empleado que herede de Usuario
 * añadir campo   salario
 * Crear método sobreescribiendo ToString
 *      tanto en Usuario como en Empleado
 * Con todos los constructores necesarios (en ambas clases)
 *  Crear un  array de usuarios, con 2 usuarios y 2 empleados
 *  Recorrer el array mostrando sus valores con ToString
 *  
 *  Al recorrer el array, si el usuario es un empleado, aumentarle
 *  el salario, y no seais tacaños
 */
namespace Ejercicio02_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Listado ");
            Usuario[] usuarios = new Usuario[4];

            usuarios[0] = new Usuario("Paula", 29, 1.7f);
            usuarios[1] = new Usuario("Sergio", 27, 1.79f);
            usuarios[2] = new Empleado();
            usuarios[2].Nombre = "Juan Eduardo";
            usuarios[2].Edad = 31;
            usuarios[2].Altura = 1.8f;
            ((Empleado) usuarios[2]).Salario = 5000;

            usuarios[3] = new Empleado("Carlos", 28, 1.69f, 25000);

            foreach (Usuario usu in usuarios)
            {
                Console.WriteLine(usu.ToString());
                if (usu.GetType() == typeof(Empleado))
                {
                    ((Empleado)usu).Salario += 1500;
                    Console.WriteLine(usu.ToString());
                }
            }
        }
    }
}
