﻿using System;

namespace Ejemplo01_Singleton
{
    class Modulo1
    {
        public static void Main2(string[] args)
        {
            GestorTextos gt = new GestorTextos();
            gt.Nuevo("AAAAA");
            gt.Nuevo("BBB");
            gt.Nuevo("CCCC");
            gt.Mostrar();
        }
    }
}
