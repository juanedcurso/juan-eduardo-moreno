﻿namespace Ejemplo03_MVC
{
    class Program
    {
        IModeloEjemplo modeloLista;
        //IModeloDicc modeloDicc;
        VistaEjemplo vista;
        //VistaEjemplo vista2;

        Program()
        {
            modeloLista = new ModeloEjemploLista();
            //modeloDicc = new ModeloEjemDiccionario();

            vista = new VistaEjemplo(new ControladorEjemplo(modeloLista));
            //vista = new VistaEjemplo(new ControladorEjemplo(modeloDicc));

        }

        void ProbarDatos()
        {
            //IModeloDicc model1 = new ModeloEjemDiccionario();
            // IModeloEjemplo model2 = new ModeloEjemplo();
            //model1.Crear(new Ejemplo(1, "Uno"));
            //model1.Crear(new Ejemplo(2, "Dos"));
            //model1.Crear(new Ejemplo(3, "tres"));

            vista.AltaEjemplo();
            vista.MostrarEjemplos();
            /*ve.MostrarUno("Tres");
            ve.MostrarUno("Estres");*/
        }

        static void Main(string[] args)
        {
            Program program = new Program();
            program.ProbarDatos();
            program.vista.Menu();
            //program.vista2.Menu();
        }


    }
}
