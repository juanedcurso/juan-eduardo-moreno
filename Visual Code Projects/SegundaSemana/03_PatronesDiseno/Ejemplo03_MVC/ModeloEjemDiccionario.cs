﻿using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    public class ModeloEjemDiccionario : IModeloDicc
    {
        Dictionary<string, Ejemplo> diccionario;

        public ModeloEjemDiccionario()
        {
            diccionario = new Dictionary<string, Ejemplo>();
        }
        public Ejemplo Crear(string key, int entero, string str)
        {
            Ejemplo ej = new Ejemplo(entero, str);
            // diccionario.TryAdd       Hace algo parecido
            if (diccionario.ContainsKey(key))
            {
                ej = null;
                // diccionario[key] = ej;   // Si quisieramos sustituir, así
            }
            else
            {
                diccionario.Add(key, ej);
            }
            return ej;
        }
        public Ejemplo Crear(int entero, string str)
        {
            string clave = str + "-" + entero;
            return Crear(clave, entero, str);
        }

        public bool BorrarUno(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombre))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;
        }

        public bool BorrarUno(int numero)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Entero == (numero))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;
        }

        public bool BorrarTodos()
        {
            foreach (var item in diccionario)
            {

                diccionario.Remove(item.Key);

            }
            return false;
        }

        public void LeerTodos(out Dictionary<string, Ejemplo> todos)
        {
            todos = diccionario;
        }

        public List<Ejemplo> LeerTodos()
        {
            List<Ejemplo> lista = new List<Ejemplo>();
            foreach (var item in diccionario)
            {
                lista.Add(item.Value);
            }
            return lista;
        }

        public Ejemplo LeerUno(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombre))
                {
                    return item.Value;
                }
            }
            return null;
        }
        public Ejemplo LeerUno(int ent)
        {
            /*TODO:
            foreach (var item in diccionario)
            {
                if (item.Value.EsEntero(ent))
                {
                    return item.Value;
                }
            }
            */
            return null;

        }

        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombreBusq))
                {
                    Ejemplo nuevoEjemploModificado = new Ejemplo(entero, str);
                    string clave = item.Key;
                    diccionario[clave] = nuevoEjemploModificado;
                    return nuevoEjemploModificado; // item.Value;
                }
            }
            return null;
        }
    }
}
