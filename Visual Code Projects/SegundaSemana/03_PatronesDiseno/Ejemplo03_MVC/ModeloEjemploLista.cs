﻿using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    class ModeloEjemploLista : IModeloEjemplo
    {
        private List<Ejemplo> ejemplos;
        public ModeloEjemploLista()
        {
            ejemplos = new List<Ejemplo>();
        }
        public Ejemplo Crear(Ejemplo ejemplo)
        {
            ejemplos.Add(ejemplo);
            return ejemplo;
        }
        public List<Ejemplo> LeerTodos()
        {
            return ejemplos;
        }
        public Ejemplo LeerUno(string str)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Str == str)
                    return ejemplo;
            }
            return null;
        }
        public Ejemplo LeerUno(int numero)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Entero == numero)
                    return ejemplo;
            }
            return null;
        }
        public bool BorrarTodos()
        {
            ejemplos = null;
            return true;
        }
        public bool BorrarUno(string nombre)
        {

            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Str.Equals(nombre))
                {
                    ejemplos.Remove(ejemplo);
                    break;
                }

            }
            return true;
        }
        public bool BorrarUno(int numero)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Entero == (numero))
                {
                    ejemplos.Remove(ejemplo);
                    break;
                }

            }
            return true;
        }

    }
}
