﻿using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    interface IVistaEjemplo
    {
        public void Menu();
        public Ejemplo AltaEjemplo();
        public List<Ejemplo> MostrarEjemplos();
        public Ejemplo MostrarUno();
        public bool EliminarUno();
        public bool EliminarTodos();
    }
}
