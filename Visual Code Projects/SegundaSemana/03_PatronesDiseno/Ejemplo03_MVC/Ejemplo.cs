﻿namespace Ejemplo03_MVC
{
    public class Ejemplo
    {
        int entero;
        string str;
        public Ejemplo(int e, string s)
        {
            Entero = e;
            Str = s;
        }

        public string Str { get => str; set => str = value; }

        public int Entero { get => entero; set => entero = value; }

        public override string ToString()
        {
            return base.ToString() + " - " + Entero + " - " + Str;
        }
    }
}
