﻿using System;
using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    class VistaEjemplo : IVistaEjemplo
    {
        ControladorEjemplo controlador;
        public VistaEjemplo(ControladorEjemplo controlador)
        {
            this.controlador = controlador;
        }
        public void Menu()
        {
            int opcion = 0;
            do
            {
                Console.WriteLine("//////////////////// MENU ////////////////////");
                Console.WriteLine("//////////////////// Alta ejemplo    1 ///////\n");
                Console.WriteLine("//////////////////// Mostrar todos   2 ///////");
                Console.WriteLine("//////////////////// Mostrar uno     3 ///////\n");
                Console.WriteLine("//////////////////// Borrar uno      4 ///////");
                Console.WriteLine("//////////////////// Borrar todos    5 ///////");
                Console.WriteLine("//////////////////// Salir           0 ///////");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {
                        case 1:
                            AltaEjemplo();
                            break;
                        case 2:
                            MostrarEjemplos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        case 4:
                            EliminarUno();
                            break;
                        case 5:
                            EliminarTodos();
                            break;
                        default:
                            Console.WriteLine("Introduzca opcion valida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                    Console.WriteLine("Escribe bien el número");
                }
            } while (opcion != 0);
        }

        public Ejemplo AltaEjemplo()
        {
            Console.WriteLine("Alta ejemplo: numero:");
            int entero = int.Parse(Console.ReadLine());
            Console.WriteLine("Alta ejemplo: string:");
            string str = Console.ReadLine();
            Ejemplo ej = controlador.AltaEjemplo(entero, str);
            return ej;
        }
        public List<Ejemplo> MostrarEjemplos()
        {
            List<Ejemplo> todos = controlador.LeerEjemplos();
            if (todos != null)
            {
                foreach (Ejemplo ejemplo in todos)
                {
                    Console.WriteLine("Ejemplo " + ejemplo.ToString());
                }
            }
            else { Console.WriteLine("Esto esta vacio..."); }
            return todos;
        }
        public Ejemplo MostrarUno()
        {
            Console.WriteLine(" 1 - Busqueda por numero ");
            Console.WriteLine(" 2 - Busqueda por nombre ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Intro numero: ");
                        string nombre = Console.ReadLine();
                        int numero = Int32.Parse(nombre);
                        Ejemplo ejemplo = controlador.LeerUno(numero);
                        if (ejemplo != null)
                            Console.WriteLine("Ejemplo " + ejemplo.ToString());
                        else
                            Console.WriteLine("No encontrado por " + nombre);
                        return ejemplo;

                    case 2:
                        Console.WriteLine("Intro nombre: ");
                        string nombre2 = Console.ReadLine();
                        Ejemplo ejemplo2 = controlador.LeerUno(nombre2);
                        if (ejemplo2 != null)
                            Console.WriteLine("Ejemplo " + ejemplo2.ToString());
                        else
                            Console.WriteLine("No encontrado por " + nombre2);
                        return ejemplo2;
                }
            }
            return null;

        }

        public bool EliminarTodos()
        {
            return controlador.BorrarTodos();
        }
        public bool EliminarUno()
        {
            Console.WriteLine(" 1 - Busqueda por numero ");
            Console.WriteLine(" 2 - Busqueda por nombre ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Intro numero: ");
                        string nombre = Console.ReadLine();
                        int numero = Int32.Parse(nombre);

                        Console.WriteLine("Al carrer el usuario numero " + numero);

                        return controlador.BorrarUno(numero);


                    case 2:
                        Console.WriteLine("Intro nombre: ");
                        string nombre2 = Console.ReadLine();

                        Console.WriteLine("Al carrer el usuario de nombre " + nombre2);
                        return controlador.BorrarUno(nombre2);

                }
            }
            return false;
        }
    }
}
