﻿using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    interface IModeloDicc
    {
        Ejemplo Crear(int entero, string str);
        List<Ejemplo> LeerTodos();
        public Ejemplo LeerUno(string nombre);
        public Ejemplo LeerUno(int entero);
        Ejemplo Modificar(string nombreBusq, int entero, string str);
        public bool BorrarUno(string nombre);
        public bool BorrarUno(int numero);
        public bool BorrarTodos();

    }
}
