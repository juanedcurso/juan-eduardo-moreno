﻿using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    interface IModeloEjemplo
    {
        public Ejemplo Crear(Ejemplo ejemplo);
        public List<Ejemplo> LeerTodos();
        public Ejemplo LeerUno(string nombre);
        public Ejemplo LeerUno(int numero);
        public bool BorrarUno(string nombre);
        public bool BorrarUno(int numero);
        public bool BorrarTodos();
    }
}
