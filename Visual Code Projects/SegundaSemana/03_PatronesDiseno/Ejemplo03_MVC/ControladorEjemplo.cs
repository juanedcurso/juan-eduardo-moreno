﻿using System.Collections.Generic;

namespace Ejemplo03_MVC
{
    class ControladorEjemplo
    {
        //IModeloDicc modelo;

        IModeloEjemplo modelo;

        public ControladorEjemplo(IModeloEjemplo modelo)
        {
            this.modelo = modelo;
        }

        public Ejemplo AltaEjemplo(int entero, string str)
        {
            return modelo.Crear(new Ejemplo(entero, str));
        }

        public List<Ejemplo> LeerEjemplos()
        {
            return modelo.LeerTodos();
        }
        public Ejemplo LeerUno(string nombre)
        {
            return modelo.LeerUno(nombre);
        }

        public Ejemplo LeerUno(int numero)
        {
            return modelo.LeerUno(numero);
        }

        internal bool BorrarUno(int numero)
        {
            return modelo.BorrarUno(numero);
        }

        internal bool BorrarUno(string nombre)
        {
            return modelo.BorrarUno(nombre);
        }

        internal bool BorrarTodos()
        {
            return modelo.BorrarTodos();
        }
    }
}
