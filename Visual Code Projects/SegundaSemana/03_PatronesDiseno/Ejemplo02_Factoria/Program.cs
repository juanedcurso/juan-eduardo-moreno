﻿using System;
using System.Collections.Generic;

namespace Ejemplo02_Factoria
{
    // https://refactoring.guru/es/design-patterns/factory-method/csharp/example

    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Vamos a la fabrica!");

            FactoriaProducto fabrica = new FactoriaProducto();
            List<Producto> lista = fabrica.CrearLista(1, 10);

            foreach (var item in lista)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }
        }
    }
}
