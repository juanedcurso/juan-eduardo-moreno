﻿using System.Collections.Generic;

namespace Ejemplo02_Factoria
{
    class FactoriaProducto
    {
        /*
        public Producto Crear()
        {
            return new Producto();
        }

        public Producto Crear(int e)
        {
            return new Producto(e);
        }*/
        public Producto Crear(int id)
        {
            string nombre;
            switch (id)
            {
                case 1: nombre = "Uno"; break;
                case 2: nombre = "Dos"; break;
                case 3: nombre = "tres"; break;
                case 4: nombre = "cuatro"; break;
                case 5: nombre = "cinco"; break;
                default: nombre = "wololo"; break;
            }
            return new Producto(id, nombre);
        }

        public List<Producto> CrearLista(int v1, int v2)
        {
            List<Producto> lis = new List<Producto>();

            while (v1 <= v2)
            {
                lis.Add(Crear(v1));
                v1++;
            }
            return lis;

        }

        public List<Producto> CrearLista(int[] ids)
        {
            List<Producto> lis = new List<Producto>();
            int count = 0;
            while (count <= ids.Length)
            {
                lis.Add(Crear(count));
                count++;
            }
            return lis;

        }

        public Producto Crear(int id, string nombre)
        {
            return new Producto(id, nombre);
        }

    }
}
