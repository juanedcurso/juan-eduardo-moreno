﻿namespace Ejemplo02_Factoria
{


    //imaginemos que esta clase viene de un dll externo.

    class Producto
    {
        int entero;
        string nombre;

        public Producto()
        {
            entero = 10;
            nombre = "Por defecto";

        }
        public Producto(int e)
        {
            entero = e;
            nombre = "Por defecto";
        }
        public Producto(int e, string s)
        {
            entero = e;
            nombre = s;
        }
        public override string ToString()
        {
            return base.ToString() + " - " + entero + " - " + nombre;
        }
    }
}
