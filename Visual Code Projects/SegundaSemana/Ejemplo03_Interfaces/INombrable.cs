﻿
namespace Ejemplo03_Interfaces
{
    /* Como un contrato que las clases que la implementen tienen que cumplir
     Cualquier clase que implemente la interfaz (NO se dice que herede)
    debe tener programados todos sus métodos y propiedades
 */
    /*
     * Está comentada porque no porque no podemeos hacer referencias
     * cruzadas entre proyectos. Entre objetos(clases) se puede hacer,
     * pero tampoco se debe, porque aumenta mucho la complejidad
     * y lo que se llama DEUDA TÉCNICA.
     * Por eso comentamos esta interfaz y sólo usamos la de 
     * Ejemplo01_Encapsulacion
     * 
    interface INombrable
    {
        string GetNombre();

        void SetNombre(string unNombre);

        string Nombre
        {
            get;
            set;
        }
    }*/
}
