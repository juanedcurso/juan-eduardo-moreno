﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public class UIConsole
    {
        public static void PedirTexto(string nombreDato, out string varDato)
        {
            Console.WriteLine(nombreDato + "? ");
            do
            {
                varDato = Console.ReadLine();
                // Validación del dato
            } while (string.IsNullOrEmpty(varDato));
        }
        // Para pasar un tipo de dato definido por el usuario del método, 
        // Están los templates, métodos genéricos
        public static void PedirNum<Tipo>(string nombreDato, out Tipo varNum)
        {
            Console.WriteLine(nombreDato + "? ");

            bool esValorOk;
            do
            {
                string str = Console.ReadLine();

                if (typeof(Tipo) == typeof(int))
                {
                    int numero;
                    esValorOk = int.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(float))
                {
                    float numero;
                    esValorOk = float.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else {
                    //TODO: cosas Use la Lista de tareas para hacer un seguimiento de los comentarios
                    //de código que usan tokens como TODO y HACK (o tokens personalizados) 
                    //esValorOk = true;
                    varNum = (Tipo)(object) 0;
                    Console.WriteLine("Error: tipo de dato no valido");
                    //Lanzamos una excepcion
                    throw new FormatException("Error: tipo de dato no valido");
                
                }
                varNum = default;
                /*
                if (!esValorOk)
                    Console.WriteLine("Pon un numero, por favor");
            */
            }
            while (!esValorOk);
        }
        public static void PedirInt(string nombreDato, out int varNum)
        {
            Console.WriteLine(nombreDato + "? ");

            bool esValorOk;
            do
            {
                string str = Console.ReadLine();

                esValorOk = int.TryParse(str, out varNum);
                if (!esValorOk)
                    Console.WriteLine("Pon un numero, por favor");
            }
            while (!esValorOk);
        }

        internal static void PedirNum<T>(string v)
        {
            throw new NotImplementedException();
        }
    }
}
