﻿using System;

namespace EjemploPasoValorRef
{
    class Program
    {
        static void Main(string[] args)
        {
            int variableEnt = 10;
            string nombre = "juan";

            Console.WriteLine("Entero dentro y antes: " + variableEnt);
            RecibimosUnaRef(ref variableEnt);
            Console.WriteLine("Entero dentro y antes: " + variableEnt);

            Cambiemos_a_mayusculas(ref nombre);
            Console.WriteLine(nombre);

            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey();
        }

        static void RecibimosUnValor(int entero)
        {
            /*  No se modifica la variable entero ( variableEnt si no le pasamos la REF ) 
             *  
            Entero dentro y antes: 10
            Entero dentro y antes: 10
            Entero dentro y despues: 21
            Entero dentro y antes: 10
            */

            //Console.WriteLine("Entero dentro y antes: " + entero);
            //entero = entero * 2 + 1;
            //Console.WriteLine("Entero dentro y despues: "+entero);
        }

        static void RecibimosUnaRef(ref int entero)
        {
           
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }

        static void Cambiemos_a_mayusculas(ref string mayusculas)
        {
            mayusculas=mayusculas.ToUpper();
        }
    }
}
