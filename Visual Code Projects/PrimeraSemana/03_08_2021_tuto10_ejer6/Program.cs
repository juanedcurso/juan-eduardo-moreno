﻿using System;

namespace tuto10_ejer6
{
    class Program
    {
        static void Main(string[] args)
        {

            int cantidad, x,y,primero,segundo,tercero,cuarto,eje;
            primero = segundo = tercero = cuarto = eje = 0;
            string linea;

            Console.Write("Ingrese la cantidad de coordenadas a examinar:");
            linea = Console.ReadLine();
            cantidad = int.Parse(linea);

            for (int i = 0; i < cantidad; i++)
            {
                Console.Write("Ingrese x de la coordenada "+(i+1)+": ");
                linea = Console.ReadLine();
                x = int.Parse(linea);

                Console.Write("Ingrese y de la coordenada "+ (i+1) + ": ");
                linea = Console.ReadLine();
                y = int.Parse(linea);

                /*
                if (x > 0 && y > 0) { Console.Write("Estamos en el primer cuadrante\n"); primero += 1; }
                else if (x > 0 && y < 0) { Console.Write("Estamos en el segundo cuadrante\n"); segundo += 1; }
                else if (x < 0 && y > 0) { Console.Write("Estamos en el cuarto cuadrante\n"); tercero += 1; }
                else if (x < 0 && y < 0) { Console.Write("Estamos en el tercer cuadrante\n"); cuarto += 1; }
                else { Console.Write("Estamos en eje\n"); eje += 1; }
                */

                //opcion simplificada
                primero += (x > 0 && y > 0) ? 1 : 0;
                segundo += (x > 0 && y < 0) ? 1 : 0;
                tercero += (x < 0 && y < 0) ? 1 : 0;
                cuarto  += (x < 0 && y > 0) ? 1 : 0;
                if (x == 0 || y==0) eje += 1;

            }

            Console.Write("Resumen: \n"+
            primero+ " en el primer cuadrante\n" +
            segundo + " en el segundo cuadrante\n" +
            tercero + " en el tercero cuadrante\n" +
            cuarto + " en el cuarto cuadrante\n" +
            eje + " en el eje");
            
            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey();

        }
    }
}
