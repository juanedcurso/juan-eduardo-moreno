﻿using System;

namespace _06_08_2021_7_EjercicoHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            /*            
            Clase empleado que herede de usuario, añadir campo salario.
            Crear metodo sobreeescribiendo toString
            tanto en usuario como en empleado
            con todos los constructores necesarios ( en ambas clases )
            crear un array de usuarios y 2 empleados
            recorrer elarray mostrando sus valores con toString.

            al recorrer el array, si el usuario es un empleado, aumentarle el salario.

            */

            _05_08_2021_5_EjercicioEncapsulacion.Usuario[] personas = new _05_08_2021_5_EjercicioEncapsulacion.Usuario[4];

            personas[0] = new _05_08_2021_5_EjercicioEncapsulacion.Usuario(1.6F, 23,"alfonsito");
            personas[1] = new _05_08_2021_5_EjercicioEncapsulacion.Usuario(1.7F, 14, "juanito");

            for (int cont = 2; cont < personas.Length; cont++)
            {
                Console.Write("Introduzca el nombre: ");
                string nombre = Console.ReadLine();

                Console.Write("Introduzca la edad: ");
                int edad = int.Parse(Console.ReadLine());

                Console.Write("Introduzca la altura (en M.): ");
                float altura = float.Parse(Console.ReadLine());
                personas[cont] = new _05_08_2021_5_EjercicioEncapsulacion.Usuario(altura,edad,nombre);
            }

            for (int cont = 0; cont < personas.Length; cont++)
            {
                personas[cont].MostrarUsuario();
            }

        }
    }
}
