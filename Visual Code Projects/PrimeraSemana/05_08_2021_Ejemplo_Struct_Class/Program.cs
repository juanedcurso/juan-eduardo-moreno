﻿using System;

namespace _05_08_2021_Ejemplo_Struct_Class
{
    class Program
    {
        static class ClaseDeLaQueNoPuedeHaberObjeto
        {
            //Solo podemos declarar variables y metodos estaticos
            static int numero;
            static void Met() { }

        }
        static void Main(string[] args)
        {
            /*
            Crear una estructura ProductoE independiente (NO anidada)
                con nombre,precio y su constructor
                con una función para mostrar sus datos

            Crearuna clase ProductoC independiente (NO anidada)
                con nombre,precio y su constructor
                con una función para mostrar sus datos
            
            Crear 4 funciones estáticas en Program:
                -Una que reciba ProductoE y modifique su nombre y su precio
                -Otra que reciba una ref ProductoE y modifique su nombre y precio
                -Una que reciba ProductoC y modifiqu su nombre y su precio
                -Otra que reciba una ref ProductoC y modifique su nombre y su precio

            Por último, en Main(), comprobar el comportamiento para saber cual
            modifica realmente la variable original.        
            */

            static void modificando1(ProductoE productoE)
            {
                productoE.nombre = "structCambia";
                productoE.precio = 10;
                
            }
            static void modificando2(ref ProductoE productoE)
            {
                productoE.nombre = "refstructCambia";
                productoE.precio = 20;
               
            }                       
            
            static void modificando3(ProductoC productoC)
            {
                productoC.nombre = "classCambia";
                productoC.precio = 30;
                
            }
            static void modificando4(ref ProductoC productoC)
            {
                productoC.nombre = "refclassCambia";
                productoC.precio = 50;                
            }

            ProductoE productoE = new ProductoE("ConStructNOcambia", 100); //el struct
            ProductoC productoC = new ProductoC("ConClassNOcambia", 200);  //la clase

            //con struct
            Console.Write("Con struct: \n");
            modificando1(productoE);
            Console.Write("nombre es :" + productoE.nombre + "\n");
            Console.Write("precio es :" + productoE.precio + "\n");
            modificando2(ref productoE);
            Console.Write("nombre es :" + productoE.nombre + "\n");
            Console.Write("precio es :" + productoE.precio + "\n");

            //con la clase
            Console.Write("Con class: \n");
            modificando3(productoC);
            Console.Write("nombre es :" + productoC.nombre + "\n");
            Console.Write("precio es :" + productoC.precio + "\n");
            modificando4(ref productoC);
            Console.Write("nombre es :" + productoC.nombre + "\n");
            Console.Write("precio es :" + productoC.precio + "\n");

            /*
            Con struct:
            nombre es :ConStructNOcambia
            precio es :100
            nombre es :refstructCambia
            precio es :20
            Con class:
            nombre es :classCambia
            precio es :30
            nombre es :refclassCambia
            precio es :50

            Diferencias:
                // referencia: https://www.geeksforgeeks.org/structure-vs-class-in-cpp/
                1) Members of a class are private by default and members of a structure are public by default. 
                2) When deriving a struct from a class/struct, the default access-specifier for a base class/struct is public.
                    And when deriving a class, the default access specifier is private. 
                3) Class can have null values but the structure can not have null values.
                4) Memory of structure is allocated in the stack while the memory of class is allocated in heap.
                5) Class requires constructor and destructor but the structure can not require it.
                6) Classes support polymorphism and also be inherited but the structure cannot be inherited.

            Comentadas por German:
            Estructuras:
                1.  Las estructuras siempre pasan por valor a menos que se indique aposta con ref que es por referencia.
                2.  No puede heredar unas de otras.
                3.  No pueden haber estructuras estaticas.
                4.  Siempre tienen un constructor por defecto.
            Clases:
                1.  Las clases siempre se pasan por referencia. Es REDUNDANTE usar ref.
                2.  Si pueden heredar unas clases de otras.
                3.  Si pueden haber clases estaticas.
                4.  Solo tienen un constructor por defecto cuando no hemos creado un constructor explicitamente.

            MIRAR https://www.campusmvp.es/recursos/post/clases-y-estructuras-en-net-cuando-usar-cual.aspx

            */

            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey();


        }


    }
    public class ProductoC
    {
        public string nombre; public int precio; 
        public ProductoC(string nombre, int precio)
        {
            this.nombre = nombre;
            this.precio = precio;          
        }

    }

    struct ProductoE
    {
        public string nombre; public int precio;
        public ProductoE(string nombre, int precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

    }

}
