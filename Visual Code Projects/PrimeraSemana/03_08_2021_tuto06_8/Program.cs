﻿using System;

namespace tuto06_8
{
    class Program
    {
        //8 - Condiciones compuestas con operadores lógicos

        static void Main(string[] args)
        {
            int num,numMax=0,numMin=0,rondas=5;
            string linea;

            /*
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            */

            for (int i = 0; i < rondas; i++)
            {
                
                Console.Write("Ingrese primer valor:");
                linea = Console.ReadLine();
                linea = linea ?? throw new ArgumentNullException(nameof(linea), "Name cannot be null");

                num = int.Parse(linea);

                
                /*
                if (num > numMax) numMax = num;
                else if (num < 0) if (num < numMin) numMin = num;
                */

                //otra forma
                _ = num > numMax ? numMax = num : 
                    (num < 0 ? (num < numMin ? numMin = num : 0) :0);

                
                bool salir = false;
                for (Console.WriteLine("Empezamos"); !salir; salir = bool.Parse(Console.ReadLine()))                    
                {
                    Console.WriteLine("¿Salimos?");
                }
                                
            }

            Console.Write("El mayor numero de los que has metido es "+numMax);
            Console.Write("\nEl menor numero de los que has metido es "+numMin);

            Console.Write($"{Environment.NewLine}Press any key to exit...");

            Console.ReadKey();
        }
    }
}
