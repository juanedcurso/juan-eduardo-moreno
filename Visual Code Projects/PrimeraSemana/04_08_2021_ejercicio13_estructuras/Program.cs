﻿using System;
using System.Collections.Generic;

namespace ejercicio13_estructuras
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombre; int edad; double altura;string aux;
            
            /*
                Crear una estructura usuario con nombre ( texto), edad (entero) y altura (decimal)
                Crear 2 variables usuarios
                Crear un array de usuarios con 4 usuarios, los 2 de antes y 2 mas que pidamos por teclado
                Crear un metodo mostrarUsuarios que muestre sus datos
                Al terminar, mostrar los 4 usuarios.
            */
            Usuario juan = new Usuario("juan", 20, 1.80);
            Usuario alfonso = new Usuario("alfoso", 20, 1.70);

            Console.Write("Ingresemos el tercer usuario...\n");
            Console.Write("Primero el nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Segundo la edad: ");
            aux = Console.ReadLine();
            edad = int.Parse(aux);
            Console.Write("Tercero la altura: ");
            aux = Console.ReadLine();
            altura = float.Parse(aux);
            Usuario lucia = new Usuario(nombre, edad, altura);

            Console.Write("Ingresemos el cuarto usuario...\n");
            Console.Write("Primero el nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Segundo la edad: ");
            aux = Console.ReadLine();
            edad = int.Parse(aux);
            Console.Write("Tercero la altura: \n");
            aux = Console.ReadLine();
            altura = float.Parse(aux);
            Usuario pepa = new Usuario(nombre, edad, altura);

            List<Usuario> usuarios = new List<Usuario>();
            usuarios.Add(juan);
            usuarios.Add(lucia);
            usuarios.Add(pepa);
            usuarios.Add(alfonso);
            
            mostrarUsuarios(usuarios);

            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey();
        }

        public static void mostrarUsuarios(List<Usuario> usuarios)
        {
            Console.Write("Mostremos los usuarios: \n\n");
            int index = 1;
            foreach (Usuario usuario in usuarios)
            {
                Console.WriteLine("El usuario numero: " + index);
                index++;
                Console.Write("El nombre es :" + usuario.nombre + "\n");
                Console.Write("La edad es :" + usuario.edad + "\n");
                Console.Write("la altura es :" + usuario.altura + "\n\n");
            }
        }

    } 
    
    public class Usuario
    {
        public string nombre; public int edad; public double altura;
        public Usuario (string nombre,int edad,double altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.altura = altura;                       

        }

    }
}
