﻿using System;

namespace Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche miCoche = new Coche();
            // miCoche.velocidad = 10; // por defecto es privado, no podemos (ni debemos) modificar directamente
            miCoche.Acelerar();
            miCoche.Acelerar();
            //seguimos sin poder modificar el valor
            //miCoche.Velocidad=100;
            miCoche.Modelo = "Kia";
            miCoche.Precio = -100;

            Console.WriteLine("Velocidad " + miCoche.Velocidad);
            Console.WriteLine("Modelo " + miCoche.Modelo);
            Console.WriteLine("Precio " + miCoche.Precio);

        }
    }
}
