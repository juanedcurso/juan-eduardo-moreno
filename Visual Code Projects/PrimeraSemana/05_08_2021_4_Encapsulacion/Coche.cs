﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Encapsulacion
{
    public class Coche
{
        float velocidad;
        string modelo;       

        //mejor que los get y set...
        //public float GetVelocidad(){return velocidad;}

        //Las propiedades, como son metodos, empiezan por mayus
        //pero como se usan como variables, van sin parentesis
        public float Velocidad
        {
            get
            {
                return velocidad;
            }
        }

        public string Modelo

        {   
            //get;
            get 
            {
                return modelo;
            }
            //set;
            set 
            {
                this.modelo = value;
            }        
        }

        float precio;
        public float Precio

        {
            get
            {
                return precio;
            }

            set
            {
                if (value >= 0)
                    precio = value;
                else
                    precio = 0;
            }
        }

        public void Acelerar() 
        {
            velocidad++;
        
        }

        public override bool Equals (object obj)
        {
            //base es como this, pero con la forma del padre, sirve para invocar a los metodos del padre.
            
            if (base.Equals(obj)) 
                return true;
            else
            {
                Coche objCoche = (Coche)obj;
                return this.Modelo == objCoche.Modelo
                    && this.Precio.Equals(objCoche.precio)      //lo ideal es usar equals en lugar de == para un string.
                    && this.Velocidad == objCoche.velocidad;
            }

        }
        public override string ToString()
        {
            return "Coche " + Velocidad + Modelo;
        }

        
        //Ejercicio: CLase CocheElectrico añadiendo campo nivelBateria double, al acelerar debemos reducir la bateria.
    }

    public class CocheElectrico : Coche
    {

        double nivelBateria;  

        public double NivelBateria
        {
            get
            {
                return nivelBateria;
            }
        }

        float precio;

        public CocheElectrico( double nivelBateria, string modelo,float precio,int velocidad )
        {          
            this.nivelBateria = nivelBateria;
            this.precio = precio;
            //this.velocidad = 
            //this.Modelo =
        }

        new public void Acelerar()
        {

            //base viene del heredado.
            base.ToString();

            nivelBateria--;

        }

        public override string ToString()
        {
            return base.ToString();    
        }
    }
}
