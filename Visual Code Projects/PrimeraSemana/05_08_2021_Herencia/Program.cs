﻿using System;

namespace _05_08_2021_6_Heredando_clase_visualcode
{
    class Program
    {
        static void Main(string[] args)
        {

            Encapsulacion.Coche miCoche = new Encapsulacion.Coche();
            Encapsulacion.Coche miCoche2 = new Encapsulacion.Coche();

            // Pese a añadir las dependencias es necesario especificar el namespace, sino no voy a poder a hacer la referencia directa.
            //_05_08_2021_5_EjercicioEncapsulacion.Usuario usuariooos = new _05_08_2021_5_EjercicioEncapsulacion.Usuario();

            if (miCoche.Equals(miCoche))
            {
                Console.WriteLine(miCoche.ToString() + "" +
                    " es igual al 2! ");
            }
            else {
                Console.WriteLine("miCoche es diferente al 2! ");
            }

            //Si quiero hacer distincion entre tipos de acelerar puedo hacer un metodo para comprobar en cual estoy

            Encapsulacion.CocheElectrico cocheElectrico = new Encapsulacion.CocheElectrico(100,"mercedes",666,1200);
            cocheElectrico.Acelerar();
            cocheElectrico.Acelerar();
            cocheElectrico.Acelerar();
            cocheElectrico.Acelerar();
            Console.WriteLine("Velocidad " + cocheElectrico.NivelBateria);
            int numero = 0;
            FuncionRecursiva(numero);


            //apuntes REPASAR metodo virtuales. metodo normal si lo machacamos da igual, si tiene la forma del padre va a llamar al metodo.

            //con new en un metodo ignoramos que sea un metodo virtual

            //Hacemos lo que se llama un casting implicito
            //usando el polimorfismo del lenguaje
            Encapsulacion.Coche miteslaComoCoche = cocheElectrico;
            double x = 3.45F;
            // lo que ocurre es que con la forma de la clase padre solo podremos usar los campos y metodos de la clase padre
            //Encapsulacion.CocheElectrico = "Tesla sencillito";
            //aunque todavia almacene los datos de Coche Electrico, no se pueden usar
            //error: miteslaComoCoche.NivelBateria=34;
            
            Object miTeslaComoObjeto = cocheElectrico;
            //error: miTeslaComoObjeto.Marca= 34;    

             
        }

        static void FuncionRecursiva(int numero)
        {
            Console.WriteLine(numero);
            FuncionRecursiva(numero+1);
        }
    }
}
