﻿using System;

namespace Cadenas
{
    class Program
    {
        static void Main(string[] args)
        {

            string texto =  "         En un lugar de La mancha de cuyo nombre me quiero acordar, pero no me acuerdo.          ";

            //quitar espacios del principio y final
            Console.WriteLine("Original: " + texto);
            Console.WriteLine("Sin espacios: " + texto.Trim());

            //mayusculas
            Console.WriteLine("Mayus: " + texto.ToUpper());
            //minusculas
            Console.WriteLine("Minus: " + texto.ToLower());

            //cortando el string
            Console.WriteLine("Minus: " + texto.Substring(20,20));

            //desde el substring 20 al final
            Console.WriteLine("Minus: " + texto.Substring(20));

            //lugar del string si coincide, si no devuelve -1
            Console.WriteLine("Minus: " + texto.IndexOf("La mancha"));

            //sustituyendo palabras
            Console.WriteLine("Por Pamplona:"
                   + texto.Replace("La mancha", "Pamplona").Replace("nombre", "NOMBRE"));



            //ejercicio: Pedir al usuario 3 palabras, y juntarlas en una unica variable de tipo texto, separandolas por comas y sin espacios.

            /*
            //numero de palabras
            string[] palabras = texto.Trim().Split(" ");
            Console.WriteLine("Palabras de la frase");
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Palabra " + p + ": " + palabras[p]);

                Console.WriteLine("Escribe pal " + p);
                palabras[p] = Console.ReadLine().Trim();
                string sus = palabras[p];
                while (sus.IndexOf("  ")>=0)
                {                   
                    sus = palabras[p].Replace("  ", " ");
                    palabras[p] = sus;

                }
                string resultado = string.Join(",",palabras);
                Console.WriteLine("Resultado: " + resultado);


            }*/

            /*
            palabras
            otra palabra
                tercera palabra
            palabra             palabra

            Resultado:
            palabra,otra palabra,tercera palabra,cuarta palabra
            */

            //version interesante

            string[] texto2 = new string[3];
            for (int i = 0; i < texto2.Length; i++)
            {
                string palabra;
                Console.Write("Escribe una palabra: ");
                palabra = Console.ReadLine();                
                texto2[i] = String.Join(" ", palabra.Trim().Split(" ", StringSplitOptions.RemoveEmptyEntries));
            }
            string resultado = String.Join(",", texto2);
            Console.WriteLine(resultado);
            

            //interesante2 falla en un caso
            /*
            Console.WriteLine("Escriba la primera palabra");
            string palabra1 = (Console.ReadLine());
            Console.WriteLine("Escriba la segunda palabra");
            string palabra2 = (Console.ReadLine());
            Console.WriteLine("Escriba la tercera palabra");
            string palabra3 = (Console.ReadLine());
            Console.WriteLine("Escriba la cuarta palabra");
            string palabra4 = (Console.ReadLine());

            string[] palabras = { palabra1.Replace("  ", string.Empty), palabra2.Replace("  ", string.Empty), palabra3.Replace("  ", string.Empty), palabra4.Replace("  ", string.Empty) };
            var resultado3 = string.Join(", ", palabras);
            Console.WriteLine("Resultado:" + resultado3);
            */

            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey();







        }
    }
}
