﻿using System;

namespace _05_08_2021_5_EjercicioEncapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Crear una clase Usuario en otro fichero, con nombre, edad, y altura
            pero con las variables miembro encapsuladas como propiedades
            nombre no puede ser ni null ni ""
            edad debe ser mayor que 0
            altura minima de 0.1F metros
            */
            string introduciendo;int edad;float altura;
            Usuario usuario = new Usuario();            

            Console.Write("Ingresemos el tercer usuario...\n");
            Console.Write("Primero el nombre: ");
            introduciendo = Console.ReadLine();
           
            usuario.Nombre = introduciendo;

            Console.Write("Segundo la edad: ");
            introduciendo = Console.ReadLine();
            edad = int.Parse(introduciendo);
            usuario.Edad = edad;

            Console.Write("Tercero la altura: ");
            introduciendo = Console.ReadLine();
            altura = float.Parse(introduciendo);            
            usuario.Altura = -100;            


            Console.WriteLine("nombre " + usuario.Nombre);
            Console.WriteLine("altura " + usuario.Altura);
            Console.WriteLine("edad " + usuario.Edad);


            /* Version german
             

             
             















            */
        }
    }
}
