﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _05_08_2021_5_EjercicioEncapsulacion
{
    public class Usuario
    {

        public override string ToString()
        {
            return base.ToString();
        }
        public void MostrarUsuario()
        {
            Console.WriteLine("");
            Console.WriteLine("=====================================================");
            Console.WriteLine("Nombre: " + nombre + ".");
            Console.WriteLine("Edad:   " + edad.ToString() + " Años");
            Console.WriteLine("Altura: " + altura.ToString() + "m.");
            Console.WriteLine("=====================================================");
        }

        bool checker;
        float altura;
        public float Altura
        {
            get
            {
                return altura;
            }

            set
            {
                if (value >= 0.1F)
                    altura = value;
                    //checker = false;
                else
                    altura = 0.1F;
            }
        }

        int edad;
        public int Edad
        {
            get
            {
                return edad;
            }

            set
            {
                //edad = value <=0 ?:value;
                if (value >= 0)
                    edad = value;
                else
                    edad = 0;
            }
        }

        string nombre;
        public string Nombre
        {            
            get
            {
                return nombre;
            }
           
            set
            {
                //string.IsNullOrEmpty(value)
                if (value !=null || value != "")
                    nombre = "SIN NOMBRE";
                    
                else
                    nombre = value;


            }
        }

        public Usuario(float altura, int edad, string nombre)
        {
            this.altura = altura;
            this.edad = edad;
            this.nombre = nombre;
        }

        public Usuario()
        {
           
        }
    }
}
