﻿using System;

namespace Tracasa
{
    class Program
    {
        static void Main(string[] args)
        {

            int numero = 10;
            //string string_fallido = 10;

            var string_num = "hola";
            string_num = 109.ToString();

            Console.WriteLine(numero + string_num);

            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            //punto de interrupcion si introduzco el nombre juan, saldra en la salida
            var currentDate = DateTime.Now;
            Console.WriteLine($"{Environment.NewLine}Hello, {name}, on {currentDate:d} at {currentDate:t}!");



            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey(true);



        }
    }
}
