﻿using System;

namespace tuto06_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1, num2;
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();

            //hay tres tipos de errores, errores de sintaxis ( tiempo compilacion ) error logico (bucles infinitos), error excepcion ( interrumpe proceso compilacion)
            num2 = int.Parse(linea);
            if (num1 > num2)
            {
                Console.Write("el mayor es " + num1);
            }
            else
            {
                Console.Write("el menor es " + num2);
            }

            Console.Write($"{Environment.NewLine}Press any key to exit...");

            Console.ReadKey();
        }
    }
}

//consejo: en modo depurar se puede mover la flecha de depuracion a la izquierda para mover el punto de depuracion.
