﻿using System;

namespace tuto06
{
    class Program
    {
        //metodos e
        static void Main(string[] args)
        {
            float sueldo;
            string linea;
            Console.Write("I987ngrese el sueldo:");
            linea = Console.ReadLine();
            sueldo = float.Parse(linea);

            {
                //esta variable es valida a nivel de bloque, fuera no EXISTE.
                string linea_en_bloque="hola";
            
            }

            //no es necesario crear un bloque de codigo, cada linea es una instruccion.
            if (sueldo > 3000)  Console.Write("Esta persona debe abonar impuestos");
            
           


            Console.Write($"{Environment.NewLine}Press any key to exit...");
            Console.ReadKey(true);
        }
    }
}
