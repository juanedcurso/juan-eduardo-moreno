﻿using OtroNameSpace;
using System;

namespace Ejercicio08_Strategy_Lambdas
{
    // MainApp Test para aplicacion
    class MainApp
    {
        static void Main()
        {
            StrategyObject estrategiaA = ConcretesStrategies.NewConcreteStrategyA("Eeeeeeooh ");
            estrategiaA.Execute();

            StrategyObject estrategiaB = ConcretesStrategies.NewConcreteStrategyB();
            estrategiaB.Execute();

            StrategyObject estrategiaC = ConcretesStrategies.NewConcreteStrategyC("Nombre directo");
            estrategiaC.Execute();
        }
    }
    static class ConcretesStrategies
    {
        // Siempre podeis hacer delegates por aquí fuera
        // Estas funciones tienen que funcionar como antes los contructores
        public static StrategyObject NewConcreteStrategyA(string mensaje)
        {
            StrategyObject strObj = new StrategyObject(mensaje);
            strObj.Type = "ConcreteStrategyA";
            /* Dinamicamente le damos la funcionalidad que antes hacíamos por herencia*/
            string textoLocal = "TEXTO LOCAL";
            strObj.Execute = () =>
            {
                strObj.RepetirChar('-', 30);
                strObj.MostrarNombre();
                Console.WriteLine("Usando var local en lambda: " + textoLocal);
            };
            // Clousure: encapsular/proteger/guardar/almacenar una variable local
            // para que posteriormente pueda usarse en una funcón lambda.
            bool boolClausurado = false;
            // Aquí estamos "clausurando" la antigua función mostrarNombre, para usarse dentro de la
            // nueva función lamda, mostrarNombre. 
            Action base_MostrarNombre = strObj.MostrarNombre;

            strObj.MostrarNombre = () =>
            {
                strObj.RepetirChar('_', 30);
                base_MostrarNombre(); // Como antes hacíamos con base.MostrarNombre();
                strObj.RepetirChar('_', 30);
                // Simplemente por usarse en una función interna a otro bloque 
                // de código, la variable persiste en el tiempo, lo que dure la función
                Console.WriteLine("Usamos la var clausurada: " + boolClausurado);
                boolClausurado = !boolClausurado;
            };
            boolClausurado = true;
            return strObj;
        }   // En teoría, al finalizar el bloque, todas las var locales se destruyen

        public static StrategyObject NewConcreteStrategyB()
        {
            StrategyObject strObj = new StrategyObject("Llamar a");
            strObj.Type = "ConcreteStrategyB";
            strObj.Execute = () =>
            {
                strObj.MostrarNombre();
                strObj.RepetirChar('\n', 3);
            };
            return strObj;
        }
        public static StrategyObject NewConcreteStrategyC(string nombre)
        {
            StrategyObject strObj = new StrategyObject();
            strObj.Type = "ConcreteStrategyC";
            strObj.GetNombre = () => nombre; // equivale a     { return nombre; }   
            //                    // Por usar nombre en una lambda, hemos clausurado el parámetro
            strObj.Execute = () =>
            {
                strObj.RepetirChar('*', 20);
                Console.WriteLine("Called ConcreteStrategyC.Execute()");
                Console.WriteLine(strObj.GetNombre());
            };
            return strObj;
        }
        static void Excute()
        {
            /*RepetirChar('-', 30);
            MostrarNombre();*/
            Console.WriteLine("Usando Excute en vez lambda: ");
        }
    }
    // Implementa el algoritmo usando el patron estrategia
    /*class ConcreteStrategyA : StrategyBase
    {
        public ConcreteStrategyA(string mensaje) : base(mensaje)
        {
        }
        public override void Execute()
        {
            RepetirChar('-', 30);
            MostrarNombre();
        }
        public override void MostrarNombre()
        {
            RepetirChar('_', 30);
            base.MostrarNombre();
            RepetirChar('_', 30);
        }
    }

    class ConcreteStrategyB : StrategyBase
    {
        public ConcreteStrategyB() : base("Llamar a")
        {
        }

        public override void Execute()
        {
            MostrarNombre();
            RepetirChar('\n', 3);
        }
    }

    class ConcreteStrategyC : StrategyBase
    {
        public ConcreteStrategyC(string nombre) : base("")
        {
            this.nombre = nombre;
        }
        public override void Execute()
        {
            RepetirChar('*', 20);
            Console.WriteLine("Called ConcreteStrategyC.Execute()");
            Console.WriteLine(this.nombre);
        }
    }
    */

}
