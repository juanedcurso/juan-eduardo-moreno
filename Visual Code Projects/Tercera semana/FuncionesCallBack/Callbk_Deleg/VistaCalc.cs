﻿using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo01_Encapsulacion;
using Ejemplo06_Funcion_Calback;
// using static Ejercicio06_Callbk_Deleg.CalculaArrays;

namespace Ejercicio06_Callbk_Deleg
{

    public static class VistaCalc
    { 
        //                                                    param1, param2, return
        // public delegate float FuncionArray(float[] array, Func<float, float, float> funOpera);

        public static void PedirArray(Func<float[], Func<float, float, float>, float> callbkCalcArray)
        {
            int cant;
            do
            {
                UIConsole.PedirNum<int>("Cantidad num", out cant);
                if (cant < 1)
                    Console.Error.WriteLine("´Minimo un elem!");
            } while (cant < 1);
            float[] arr = new float[cant];

            for (int i = 0; i < arr.Length; i++)
            {
                UIConsole.PedirNum<float>("Num " + (i + 1) + "º", out arr[i]);
            }
            char operacion;
            Dictionary<char, Func<float, float, float>> operaciones = new Dictionary<char, Func<float, float, float>>();
            operaciones.Add('+', Calculadora_B.SumarB);
            // En lugar como función callback, una función estática, pública, y con nombre,
            // Podemos usar funciones sin nombre, o anónimas o también llamadas funciones lambda
            operaciones.Add('-', (float x, float y) => { return x - y; });
            // Cuando tenemos una sola línea de código con el return, nos podemos ahorrar
            // las llaves, y el return.
            Func<float, float, float> funMult = (float x, float y) => x * y;

            operaciones.Add('*', funMult);

            // operaciones.Add('/', Calculadora_B.DividirB);
            operaciones.Add('/', (float x, float y) =>
            {   
                // Las llaves de bloque son obligatorias con más de una línea.
                if (y == 0)
                    throw new System.DivideByZeroException("Se trató de dividir por 0.");

                return x / y;
            });
                
            do
            {
                Console.WriteLine("Escriba la operacion (+ - * / ): ");
                operacion = Console.ReadKey().KeyChar;
            } while ( ! operaciones.ContainsKey(operacion));            

            Console.WriteLine("Result: " + callbkCalcArray?.Invoke(arr, operaciones[operacion]));

        }
    }
}
