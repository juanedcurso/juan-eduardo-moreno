﻿namespace EjercicioPropuesto
{
    class Sabio
    {
        static public string Suma(string[] numeritos)
        {
            float aux = 0;
            foreach (var item in numeritos)
            {
                aux += float.Parse(item);

            }
            return $" La suma es: {aux} ";

        }

        static public string Mult(string[] numeritos)
        {
            float aux = 1;

            foreach (var item in numeritos)
            {

                aux *= float.Parse(item);


            }
            return $" La multiplicacion es: {aux} ";

        }

        static public string Resta(string[] numeritos)
        {
            float aux = 0;
            int aux2 = 0;
            foreach (var item in numeritos)
            {
                if (aux2 != 0)
                {
                    aux -= float.Parse(item);

                }
                else { aux = float.Parse(item); aux2++; }
            }
            return $" La resta es: {aux} ";

        }
    }
}
