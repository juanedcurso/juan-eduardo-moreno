﻿using System;
using System.Linq;

namespace EjercicioPropuesto
{
    class Program
    {

        delegate string FuncionPensando(string[] numeritos);
        static void Main(string[] args)
        {
            Console.WriteLine("Hagamos unas cuentas...");

            /*
             Ejercici: crear dos sistemas (clases independientes)
            la primera con 4 funciones sumar restar mult y div
            todo con float. Estas podran realizar operaciones sobre array
            {9,9,9,7} suma -> resta -> mul->
            {3} devolvera 3 siempre.
            no puede estar vacio el array
            la segunda clase pide al usuario cuantos operandos habra
            y hara las cuentas.                  
             */

            CalculosDeSabio(Sabio.Suma);
            CalculosDeSabio(Sabio.Resta);
            CalculosDeSabio(Sabio.Mult);

            // he estado buscando la manera de hacer el 4+4+4+4+4 o algo similar en funcionesCallBack

        }

        static void CalculosDeSabio(FuncionPensando operador)
        {

            Console.WriteLine("Meteme el array");
            string cadena = Console.ReadLine();

            string[] separadores = { "{", ",", "}", "" };
            string[] resultadoSeparado = cadena.Split(separadores, StringSplitOptions.None);

            resultadoSeparado = resultadoSeparado.Where((source, index) => index != 0).ToArray();
            resultadoSeparado = resultadoSeparado.Where((source, index) => index != resultadoSeparado.Length - 1).ToArray();

            string final = operador(resultadoSeparado);

            Console.WriteLine(final);
        }
    }
}
