﻿using System;

namespace Wikipedia.Patterns.Strategy
{
    // MainApp Test para aplicacion
    class MainApp
    {
        static void Main()
        {
            Context context;

            string mensaje = "wololooooooooooooo";
            // Tres contextos con diferentes estrategias
            context = new Context(new ConcreteStrategyA(mensaje));
            context.Execute();

            context = new Context(new ConcreteStrategyB());
            context.Execute();

            context = new Context(new ConcreteStrategyC(mensaje));
            context.Execute();

        }
    }
    // Una clase abstracta lo principal es que no puede ser instanciada como las interfaces,
    // y puede tener algunos métodos implementados y otros sin implementar(declarando sólo su interfaz). Estos métodos son abstractos.
    abstract class StrategyBase
    {
        protected string nombre;
        public StrategyBase(string mensaje) { nombre = mensaje + " " + GetType().Name + " Execute"; }

        protected void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        public abstract void Execute();

        public virtual void MostrarNombre()
        {
            Console.WriteLine(nombre);
        }
    }

    // Implementa el algoritmo usando el patron estrategia
    class ConcreteStrategyA : StrategyBase
    {
        public ConcreteStrategyA(string mensaje) : base(mensaje) { }

        public override void Execute()
        {
            RepetirChar('-', 30);
            Console.WriteLine("Called ConcreteStrategyA.Execute()");
        }

        //en caso de no hacerlo virtual :'function1' : cannot override inherited member 'function2' because it is not marked "virtual", "abstract", or "override"
        public override void MostrarNombre() { }
    }

    class ConcreteStrategyB : StrategyBase

    {
        public ConcreteStrategyB() : base("LLamar a")
        {

        }
        public override void Execute()
        {
            RepetirChar('*', 30);
            Console.WriteLine("Called ConcreteStrategyB.Execute()");
        }
    }

    class ConcreteStrategyC : StrategyBase
    {
        public ConcreteStrategyC(string nombre) : base("")
        {
            this.nombre = nombre;
        }
        public override void Execute()
        {
            for (int i = 0; i < 20; i++)
            {
                Console.Write("*_*");
            }
            Console.WriteLine("\nCalled ConcreteStrategyC.Execute()");
        }
    }

    // Contiene un objeto ConcreteStrategy y mantiene una referencia a un objeto Strategy
    class Context
    {
        StrategyBase strategy;

        // Constructor
        public Context(StrategyBase strategy)
        {
            this.strategy = strategy;
        }

        public void Execute()
        {
            strategy.Execute();
        }
    }
}
