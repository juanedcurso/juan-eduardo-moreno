﻿using System;
using System.Linq;

namespace FuncionesCallBack
{
    class Calculadora
    {

        static public float SumarA(float a1, float a2)
        {
            return a1 + a2;
        }

        static public float Multiplicar(float a1, float a2)
        {
            return a1 * a2;
        }
        static public float OperacionPorCadena(string cadena)
        {

            string[] separadores = { "*", "+", "-" };
            string[] separadores2 = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            string[] resultadoSeparado = cadena.Split(separadores, StringSplitOptions.None);
            string[] resultadoSeparado2 = cadena.Split(separadores2, StringSplitOptions.None);


            resultadoSeparado2 = resultadoSeparado2.Where((source, index) => index != 0).ToArray();
            resultadoSeparado2 = resultadoSeparado2.Where((source, index) => index != resultadoSeparado2.Length - 1).ToArray();

            float final = 0;
            int j = 0;
            for (int i = 0; i <= resultadoSeparado.Length; i++)
            {
                if (i % 2 == 0)
                {
                    switch (resultadoSeparado2[j])
                    {
                        case "*":
                            final = float.Parse(resultadoSeparado[i - 1]) * float.Parse(resultadoSeparado[i + 1]);

                            break;
                        case "+":
                            final = float.Parse(resultadoSeparado[i - 1]) + float.Parse(resultadoSeparado[i + 1]);

                            break;
                        case "-":
                            final = float.Parse(resultadoSeparado[i - 1]) - float.Parse(resultadoSeparado[i + 1]);

                            break;
                        default:
                            break;
                    }
                    j++;
                }

            }

            return final;

        }

    }
}
