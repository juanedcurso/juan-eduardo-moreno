﻿using System;

namespace FuncionesCallBack
{
    /*
     creamos un nuevo tipo de dato qu eindica que es una funcion
    ni clase ni interfaz... donde lo que importa son los tipos de datos que recibe y devuelve
    es decir declaramos un delegado     
     */
    delegate float FuncionOperador(float op1, float op2);
    delegate float FuncionOperadorInteractivo(string cadena);
    //poddremos crear variables de tipo float funcion (float,float)
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Console.WriteLine("multiplicador");
            VistaCalculadora(Calculadora.Multiplicar);
            Console.WriteLine("sumar");
            VistaCalculadora(Calculadora.SumarA);
            */
            Console.WriteLine("Calculadora interactiva");
            VistaCalculadoraInteractiva(Calculadora.OperacionPorCadena);
        }
        //ej1 usar vistacalculadora con la calculadora creada por vosotros
        //pero exacta. Llamad a la clase calculadoraB 
        //ej2 crear una vista operadora (fun estatica) que el usuario pueda
        // introducir una unica cadena con dos numeros y el operador.
        //que calcule usando cala o calcb u otra diferente: si lo pone mal que diga
        //si lo pone mal que diga no te entiendo.
        //ejemplos 10+22 o 333*1 o 2.5*11


        // esta funcion necesita de una funcion callback para
        //saber como tiene que calcular

        static void VistaCalculadora(FuncionOperador operador)
        {
            Console.WriteLine("Operando 1:");
            float x = float.Parse(Console.ReadLine());
            Console.WriteLine("Operando 2:");
            float y = float.Parse(Console.ReadLine());
            float resultado = operador(x, y);
            Console.WriteLine(resultado);
        }

        static void VistaCalculadoraInteractiva(FuncionOperadorInteractivo operador)
        {
            Console.WriteLine("Meteme la cadena");
            string cadena = Console.ReadLine();
            float final = operador(cadena);



            Console.WriteLine(final);
        }

    }
}
